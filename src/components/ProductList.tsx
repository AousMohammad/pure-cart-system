/**
 * ProductList Component
 * 
 * A UI component that renders a list of products in a grid layout. Each product is represented
 * by a `ProductCard` which displays product details and provides actions such as adding the product
 * to the cart.
 * 
 * Features:
 * - Displays products in a grid layout.
 * - Handles empty product lists by displaying a message to the user.
 * - Integrates with the global context to fetch product states and update them.
 * 
 * Props:
 * - `products`: An array of products to be displayed. Each product should adhere to the `Products` type.
 * 
 * Usage:
 * <ProductList products={productsArray} />
 * 
 * Dependencies:
 * - `ProductCard`: A component that represents an individual product.
 * - `ProductState`: Type definitions for the state of a product (e.g., NORMAL, ADDED, BOUGHT).
 * - `useGlobalContext`: A custom hook to access the global context.
 * - `Products`: Type definitions for products.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react'
import { ProductCard } from './Product';
import { ProductState } from './Product/types';
import { useGlobalContext } from '../context';
import { Products } from '../types';


type Props = {
    products: Products[];
}
export const ProductList: React.FC<Props> = ({ products }) => {

    const { addToCart, productStates, updateProductState } = useGlobalContext();

    return (
        <div className='grid-container'>
            {products?.length === 0 && <p>No products available.</p>}
            {products?.map(product => (
                <div key={product.id}>
                    <ProductCard
                        product={product}
                        onAddToCart={addToCart}
                        updateProductState={updateProductState}
                        state={productStates[product.id] || ProductState.NORMAL}
                    />
                </div>
            ))}
        </div>
    );
}