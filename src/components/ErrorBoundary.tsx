/**
 * ErrorBoundary Component
 * 
 * A higher-order component designed to catch JavaScript errors anywhere in its child component tree. 
 * It provides a fallback UI and utilizes the global context to show toast notifications upon encountering errors.
 * 
 * Features:
 * - Captures errors in the child component tree using React's error boundary mechanism.
 * - Displays a fallback UI to inform users when an error occurs.
 * - Utilizes the global context's `showToast` method to display toast notifications for errors.
 * 
 * Props:
 * - `children`: The React elements that are intended to be rendered within this error boundary's scope.
 * 
 * Returns:
 * - Either the children components if no error occurs or a fallback UI in case of errors.
 * 
 * Usage:
 * ```jsx
 * <ErrorBoundary>
 *   <SomeComponent />
 * </ErrorBoundary>
 * ```
 * 
 * Dependencies:
 * - `GlobalContext`: The context that provides access to the global state and methods.
 * - `GlobalContextProps`: Type definitions delineating the structure of the global context.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';
import { GlobalContext } from '../context/GlobalContext';
import { GlobalContextProps } from '../types';


type Props = {
    children: React.ReactElement;
}

export class ErrorBoundary extends React.Component<Props, { hasError: boolean }> {
    static contextType = GlobalContext;  // Set contextType to GlobalContext
    context!: GlobalContextProps;  // Explicitly type the context
    state = { hasError: false };

    static getDerivedStateFromError(error: Error) {
        return { hasError: true };
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
        console.error("Caught an error:", error, errorInfo);
        if (this.context && this.context.showToast) {
            this.context.showToast('An error occurred!', 'error');
        }
    }

    render() {
        if (this.state.hasError) {
            return <p>Error: Something went wrong!</p>;
        }

        return this.props.children;
    }
}
