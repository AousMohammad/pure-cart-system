/**
 * useCartLogic Hook
 * 
 * This custom hook provides logic related to the shopping cart operations.
 * It handles product quantity changes, calculates the total price of items in the cart,
 * and exposes methods to clear the cart and remove items from it.
 * 
 * Features:
 * - Handles the logic for changing the quantity of a product in the cart.
 * - Calculates the total price of all items in the cart.
 * 
 * Dependencies:
 * - `useGlobalContext`: A custom hook to access the global context, which provides cart-related methods and state.
 * 
 * Returns:
 * - `cart`: The current state of the shopping cart.
 * - `clearCart`: A method to clear all items from the cart.
 * - `handleQuantityChange`: A method to handle the quantity change of a product.
 * - `removeFromCart`: A method to remove a specific product from the cart.
 * - `total`: The total price of all items in the cart.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import { useGlobalContext } from '../../../context';
import { Products } from '../../../types';

export const useCartLogic = () => {
    const { cart, removeFromCart, clearCart, updateProductQuantity } = useGlobalContext();

    const handleQuantityChange = (product: Products, change: number) => {
        const newQuantity = (product?.quantity || 1) + change;
        if (newQuantity <= 0) {
            removeFromCart(product.id);
        } else {
            updateProductQuantity(product.id, newQuantity);
        }
    };

    const total = cart.reduce((acc, product) => acc + (product.price * (product.quantity || 1)), 0);

    return { cart, clearCart, handleQuantityChange, removeFromCart, total };
};
