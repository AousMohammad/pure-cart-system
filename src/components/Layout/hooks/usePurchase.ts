/**
 * usePurchase Custom Hook
 * 
 * This hook provides functionality to handle the purchase process. It updates the state of products in the cart to "bought",
 * clears the cart, and provides feedback to the user.
 * 
 * @param {any[]} cart - An array of products currently in the cart.
 * @param {() => void} clearCart - A function to clear all products from the cart.
 * 
 * @returns {() => Promise<void>} A function that handles the purchase process when invoked.
 * 
 * Dependencies:
 * - `useGlobalContext`: A custom hook to access the global context.
 * - `simulateApiDelay`: A utility function to simulate an API delay.
 * - `ProductState`: Type definitions for the state of a product (e.g., NORMAL, ADDED, BOUGHT).
 * 
 * Usage:
 * ```tsx
 * const handlePurchase = usePurchase(cart, clearCart);
 * ```
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';
import { useGlobalContext } from '../../../context';
import { simulateApiDelay } from '../../../utils';
import { ProductState } from '../../Product/types';

export const usePurchase = (cart: any[], clearCart: () => void, toggleSidebar: () => void) => {
    const [loading, setLoading] = React.useState<boolean>(false);
    const { updateProductState, showToast } = useGlobalContext();

    const handlePurchase = async () => {
        setLoading(true);
        try {
            await simulateApiDelay();
            setLoading(false);
            toggleSidebar();
            cart.forEach(product => {
                updateProductState(product.id, ProductState.BOUGHT);
            });

            clearCart();
            showToast('Purchase successful!', 'success');
        } catch (error) {
            setLoading(false);
            toggleSidebar();
            showToast('Purchase failed. Please try again.', 'error');
        }
    };

    return { handlePurchase, loading };
};
