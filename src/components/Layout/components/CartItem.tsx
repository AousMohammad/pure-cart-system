/**
 * CartItem Component
 * 
 * This component represents an individual item in the cart. It displays the product's
 * title, price, and quantity. Users can increase or decrease the quantity of the product
 * or remove the product from the cart.
 * 
 * Props:
 * - product: The product object containing details like title, price, and quantity.
 * - handleQuantityChange: A function to handle the change in quantity of the product.
 * - removeFromCart: A function to remove the product from the cart.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 * 
 * @param {CartItemProps} props - The props passed to the component.
 * 
 * @returns {React.FC<CartItemProps>} The CartItem component.
 */

import React from 'react';
import { Products } from '../../../types';

type CartItemProps = {
    product: Products;
    handleQuantityChange: (product: Products, change: number) => void;
    removeFromCart: (id: number) => void;
};

export const CartItem: React.FC<CartItemProps> = ({ product, handleQuantityChange, removeFromCart }) => {
    return (
        <div key={product.id}>
            <span>{product.title}</span>
            <span>${product.price.toFixed(2)}</span>
            <button onClick={() => handleQuantityChange(product, -1)}>-</button>
            <span>{product?.quantity || 1}</span>
            <button onClick={() => handleQuantityChange(product, 1)}>+</button>
            <button className='danger' onClick={() => removeFromCart(product.id)}>Delete</button>
        </div>
    );
};
