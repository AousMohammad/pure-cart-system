/**
 * Sidebar Component
 * 
 * This component represents the shopping cart sidebar of the application. It displays a list of 
 * products added to the cart, their quantities, and the total price. Users can adjust the quantity 
 * of each product, remove products from the cart, or proceed to purchase.
 * 
 * Features:
 * - Displays a list of products in the cart using the `CartItem` component.
 * - Provides functionality to adjust product quantity and remove products.
 * - Displays the total price of items in the cart.
 * - Allows users to clear the cart and proceed with the "Purchase" button.
 * 
 * Dependencies:
 * - `useSidebarContext`: A custom hook to access the sidebar context, which provides functionality to toggle the sidebar.
 * - `CartItem`: A component that represents an individual product in the cart.
 * - `useCartLogic`: A custom hook that provides logic related to cart operations.
 * - `usePurchase`: A custom hook that provides logic to handle the purchase process.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';
import { useSidebarContext } from '../../../context';
import { CartItem } from './CartItem';
import { useCartLogic } from '../hooks/useCartLogic';
import { usePurchase } from '../hooks/usePurchase';
import { Loader } from '../../Loader';

export const Sidebar: React.FC = () => {
    const { isSidebarOpen, toggleSidebar } = useSidebarContext();
    const { cart, clearCart, handleQuantityChange, removeFromCart, total } = useCartLogic();
    const { handlePurchase, loading } = usePurchase(cart, clearCart, toggleSidebar);

    return (
        <>
            {loading && (<Loader />)}
            <aside className={`sidebar ${isSidebarOpen ? 'open' : ''}`}>
                <button className="close-btn" onClick={toggleSidebar}>&times;</button>
                {cart.map(product => (
                    <CartItem
                        key={product.id}
                        product={product}
                        handleQuantityChange={handleQuantityChange}
                        removeFromCart={removeFromCart}
                    />
                ))}
                <div>Total: ${total.toFixed(2)}</div>
                <button onClick={handlePurchase}>Purchase</button>
            </aside>
        </>
    );
};

