/**
 * Layout Component
 * 
 * This component serves as the main layout wrapper for the application. It includes the header, 
 * main content area, and the shopping cart sidebar. The layout adjusts based on whether the 
 * sidebar is open or closed.
 * 
 * Features:
 * - Displays the `Header` component at the top.
 * - Wraps the main content of the application, which is passed as children.
 * - Displays the `Sidebar` component, representing the shopping cart.
 * - Adjusts the main content area's styling based on the sidebar's open/close state.
 * 
 * Dependencies:
 * - `Header`: The main header component of the application.
 * - `Sidebar`: The shopping cart sidebar component.
 * - `useSidebarContext`: A custom hook to access the sidebar context, which provides the state of the sidebar (open/closed).
 * 
 * Props:
 * - `children`: The main content to be rendered within the layout.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';
import { Header, Sidebar } from '../components';
import "../styles/styles.scss";
import { useSidebarContext } from '../../../context';

type LayoutProps = {
    children: React.ReactNode;
}

export const Layout: React.FC<LayoutProps> = ({ children }) => {
    const { isSidebarOpen } = useSidebarContext();
    return (
        <div className="layout">
            <Header />
            <main className={`content ${isSidebarOpen ? 'sidebar-open' : ''}`}>
                {children}
            </main>
            <Sidebar />
        </div>
    );
}
