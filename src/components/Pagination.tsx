/**
 * Pagination Component
 * 
 * A UI component that provides pagination controls for navigating through items.
 * 
 * Features:
 * - Displays pagination buttons based on the total number of items and items per page.
 * - Highlights the current page button.
 * - Provides "Previous" and "Next" buttons for easier navigation.
 * - Disables "Previous" button on the first page and "Next" button on the last page.
 * 
 * Props:
 * - `currentPage`: The current active page number.
 * - `totalItems`: The total number of items available.
 * - `itemsPerPage`: The number of items displayed per page.
 * - `onPageChange`: A callback function that gets triggered when a page is changed.
 * 
 * Returns:
 * - A component that displays pagination controls based on the provided props.
 * 
 * Usage:
 * ```jsx
 * <Pagination 
 *     currentPage={1} 
 *     totalItems={100} 
 *     itemsPerPage={10} 
 *     onPageChange={(page) => console.log(page)}
 * />
 * ```
 * 
 * Dependencies:
 * - None.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';

type PaginationProps = {
    currentPage: number;
    totalItems: number;
    itemsPerPage: number;
    onPageChange: (page: number) => void;
};

export const Pagination: React.FC<PaginationProps> = ({ currentPage, totalItems, itemsPerPage, onPageChange }) => {
    const totalPages = Math.ceil(totalItems / itemsPerPage);

    return (
        <div className="pagination">
            <button disabled={currentPage === 1} onClick={() => onPageChange(Math.max(currentPage - 1, 1))}>
                Previous
            </button>

            {Array.from({ length: totalPages }).map((_, index) => (
                <button
                    key={index}
                    className={currentPage === index + 1 ? 'active' : ''}
                    onClick={() => onPageChange(index + 1)}
                >
                    {index + 1}
                </button>
            ))}

            <button disabled={currentPage === totalPages} onClick={() => onPageChange(currentPage + 1)}>
                Next
            </button>
        </div>
    );
};
