/**
 * Toaster Component
 * 
 * A UI component that displays toast notifications to provide feedback to the user. 
 * Toasts are small, non-disruptive messages that appear at the top or bottom of the screen 
 * to inform the user of an operation's status (e.g., success, error).
 * 
 * Features:
 * - Displays a message to the user.
 * - Supports different types (e.g., success, error) to visually indicate the nature of the message.
 * - Uses CSS for styling and positioning.
 * 
 * Props:
 * - `message`: The content of the toast notification.
 * - `type`: The type of the toast, which determines its visual appearance. Supported types include 'success', 'error', 'info', and 'warning'.
 * 
 * Usage:
 * <Toaster message="Item added to cart!" type="success" />
 * 
 * Dependencies:
 * - `ToasterProps`: Type definitions for the props of the Toaster component.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';
import { ToasterProps } from '../../../types';
import '../styles/toaster.scss';

export const Toaster: React.FC<ToasterProps> = ({ message, type }) => {
    return (
        <div className={`toaster ${type} ${type ? 'visible': ''}`}>
            {message}
        </div>
    );
};