/**
 * ProductCard Component
 * 
 * This component displays a product card with details such as the product image, title, price, state, and description.
 * It also provides action buttons based on the current state of the product, such as "Add to Cart" or indicating that the product has been added or bought.
 * 
 * Features:
 * - Displays product details like image, title, price, and description.
 * - Uses the `ProductImage` component to display the product image with error handling.
 * - Uses the `useProductActions` hook to handle product actions like adding to cart.
 * - Renders different action buttons based on the current state of the product.
 * 
 * Props:
 * - `product`: The product object containing details like images, title, price, and description.
 * - `onAddToCart`: A callback function to handle the action when a product is added to the cart.
 * - `state`: The current state of the product (e.g., NORMAL, ADDED, BOUGHT).
 * - `updateProductState`: A callback function to update the state of the product.
 * 
 * Dependencies:
 * - `ProductImage`: A component to display the product image with error handling.
 * - `useProductActions`: A custom hook to handle product actions.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';
import '../styles/productCard.scss';
import { ProductCardProps, ProductState } from '../types';
import { ProductImage } from '../components';
import { useProductActions } from '../hooks';

export const ProductCard: React.FC<ProductCardProps> = ({ product, onAddToCart, state, updateProductState }) => {
    const { handleAddToCart } = useProductActions(product, onAddToCart, updateProductState, state);

    return (
        <div className="product-card">
            {product.images[0] && <ProductImage src={product.images[0]} alt={product.title} />}
            <div className="product-name">{product.title}</div>
            <div className="product-price">${product.price.toFixed(2)}</div>
            <div className={`product-state ${state}`}>State: {state}</div>
            <div className="product-description">{product.description}</div>
            {state === ProductState.NORMAL && (
                <button onClick={() => handleAddToCart(ProductState.ADDED)}>
                    Add to Cart
                </button>
            )}
            {state === ProductState.ADDED && (
                <button className="success" onClick={() => handleAddToCart(ProductState.ADDED)}>
                    it's added to Cart
                </button>
            )}
            {state === ProductState.BOUGHT && (
                <button className="info" onClick={() => handleAddToCart(ProductState.BOUGHT)}>
                    you bought this item
                </button>
            )}
        </div>
    );
};
