import { Products } from "../../../types";

export enum ProductState {
    NORMAL = "normal",
    ADDED = "added",
    BOUGHT = "bought"
}

export interface ProductCardProps {
    product: Products;
    updateProductState: (productId: number, state: ProductState) => void;
    onAddToCart: (product: Products) => void;
    state: ProductState;
}