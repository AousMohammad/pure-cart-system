/**
 * useProductActions Hook
 * 
 * This custom hook provides logic and actions related to product operations, such as adding a product to the cart.
 * It handles the action of adding a product to the cart and updating its state, while also providing feedback to the user through toast notifications.
 * 
 * Features:
 * - Handles the action of adding a product to the cart.
 * - Checks if the product is already added to the cart and provides feedback through a toast notification.
 * - Updates the state of the product after adding it to the cart.
 * 
 * Parameters:
 * - `product`: The product object containing details like id, images, title, price, and description.
 * - `onAddToCart`: A callback function to handle the action when a product is added to the cart.
 * - `updateProductState`: A callback function to update the state of the product.
 * - `state`: The current state of the product (e.g., NORMAL, ADDED, BOUGHT).
 * 
 * Returns:
 * - `handleAddToCart`: A function that handles the action of adding a product to the cart and updating its state.
 * 
 * Dependencies:
 * - `useGlobalContext`: A custom hook that provides access to the global context, including the `showToast` function for displaying toast notifications.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import { ProductState } from '../types';
import { Products } from '../../../types';
import { useGlobalContext } from '../../../context';

export const useProductActions = (product: Products, onAddToCart: Function, updateProductState: Function, state: ProductState) => {
    const { showToast } = useGlobalContext();

    const handleAddToCart = (newState: ProductState) => {
        if (state === ProductState.ADDED) {
            showToast("Error: Product is already in the cart!", "error");
            return;
        }
        showToast("added to cart", "success");
        onAddToCart(product);
        updateProductState(product.id, newState);
    };

    return { handleAddToCart };
};
