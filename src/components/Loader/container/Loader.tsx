/**
 * Loader Component
 * 
 * This component displays a loading spinner. The size and color of the spinner can be customized.
 * 
 * Features:
 * - Displays a loading spinner.
 * - Allows customization of spinner size and color.
 * 
 * Props:
 * - `size`: The size of the loader. Determines the size class (small, medium, large).
 * - `color`: The color of the loader.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react';
import { LoaderProps } from '../types';
import '../styles/styles.scss';

export const Loader: React.FC<LoaderProps> = ({ size = 50, color = "#000" }) => {
    let sizeClass = "medium";

    if (size <= 30) sizeClass = "small";
    else if (size >= 70) sizeClass = "large";

    return (
        <div className='lodaer-container'>
            <div className={`loader ${sizeClass}`} style={{ color }}></div>
        </div>
    );
};
