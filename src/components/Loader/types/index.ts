export interface LoaderProps {
    size?: number; // Size of the loader in pixels
    color?: string; // Color of the loader
}
