import React from 'react';
import { GlobalProvider } from './context';
import "./styles/styles.scss";
import { Home } from './pages';

function App() {
  return (
    <GlobalProvider>
      <div className="App">
        <Home />
      </div>
    </GlobalProvider>
  );
}

export default App;
