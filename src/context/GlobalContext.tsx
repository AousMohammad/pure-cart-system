/**
 * GlobalProvider Component
 * 
 * A higher-order component that serves as a context provider, wrapping the entire application to offer global state and functionalities.
 * It efficiently manages product states, cart operations, and toast notifications.
 * 
 * Features:
 * - Offers a centralized global state and utility methods to handle product states and cart operations.
 * - Leverages custom hooks, namely `useToast` and `useCart`, to streamline toast notifications and cart-related logic.
 * - Dynamically displays toast notifications based on specific events or errors.
 * 
 * Props:
 * - `children`: The React elements that are intended to be rendered within this provider's scope.
 * 
 * Returns:
 * - A context provider component that encapsulates the provided children, granting them access to the global state and associated methods.
 * 
 * Usage:
 * ```jsx
 * <GlobalProvider>
 *   <App />
 * </GlobalProvider>
 * ```
 * 
 * Dependencies:
 * - `GlobalContextProps`: Type definitions delineating the structure of the global context.
 * - `Toaster`: A component responsible for rendering toast notifications.
 * - `ProductState`: Type definitions specifying the various states a product can be in.
 * - `useCart`: A custom hook that encapsulates cart-related logic.
 * - `useToast`: A custom hook dedicated to handling toast notifications.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React, { createContext, useContext, useCallback, useState } from 'react';
import { useCart, useToast } from '../hooks';
import { Toaster } from '../components';
import { GlobalContextProps } from '../types';
import { ProductState } from '../components/Product/types';

export const GlobalContext = createContext<GlobalContextProps | undefined>({
    cart: [],
    productStates: {},
    updateProductState: () => { },
    addToCart: () => { },
    removeFromCart: () => { },
    clearCart: () => { },
    updateProductQuantity: () => { },
    showToast: () => { }
});

export const useGlobalContext = () => {
    const context = useContext(GlobalContext);
    if (!context) {
        throw new Error('useGlobalContext must be used within a GlobalProvider');
    }
    return context;
};

type Props = {
    children: React.ReactElement;
}

export const GlobalProvider: React.FC<Props> = ({ children }) => {
    const [productStates, setProductStates] = useState<Record<number, ProductState>>({});

    const updateProductState = useCallback((productId: number, state: ProductState) => {
        setProductStates(prevStates => ({
            ...prevStates,
            [productId]: state
        }));
    }, []);

    const { toastMessage, toastType, showToast } = useToast();
    const { cart, addToCart, removeFromCart, clearCart, updateProductQuantity } = useCart(showToast, updateProductState);

    return (
        <GlobalContext.Provider value={{ cart, addToCart, showToast, removeFromCart, clearCart, updateProductQuantity, productStates, updateProductState }}>
            {children}
            {toastMessage && <Toaster message={toastMessage} type={toastType} />}
        </GlobalContext.Provider>
    );
};

