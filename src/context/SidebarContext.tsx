/**
 * SidebarProvider Component
 * 
 * A context provider component that wraps parts of the application to provide state and functionalities related to the sidebar.
 * It manages the state of the sidebar (open or closed) and provides a method to toggle this state.
 * 
 * Features:
 * - Provides state to determine if the sidebar is open or closed.
 * - Provides a method to toggle the sidebar's state.
 * 
 * Props:
 * - `children`: React elements that will be rendered inside this provider.
 * 
 * Returns:
 * - A context provider that wraps the given children and provides them access to the sidebar state and toggle method.
 * 
 * Usage:
 * <SidebarProvider>
 *   <App />
 * </SidebarProvider>
 * 
 * Dependencies:
 * - `SidebarContextProps`: Type definitions for the sidebar context.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React, { createContext, useContext, useState } from 'react';

interface SidebarContextProps {
    isSidebarOpen: boolean;
    toggleSidebar: () => void;
}

const SidebarContext = createContext<SidebarContextProps | undefined>(undefined);

export const useSidebarContext = () => {
    const context = useContext(SidebarContext);
    if (!context) {
        throw new Error('useSidebarContext must be used within a SidebarProvider');
    }
    return context;
};

type Props = {
    children: React.ReactNode;
}

export const SidebarProvider: React.FC<Props> = ({ children }) => {
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);

    const toggleSidebar = React.useCallback(() => {
        setIsSidebarOpen(prev => !prev);
    }, []);

    return (
        <SidebarContext.Provider value={{ isSidebarOpen, toggleSidebar }}>
            {children}
        </SidebarContext.Provider>
    );
};
