import { ProductState } from "../components/Product/types";

export type Products = {
    id: number;
    title: string;
    price: number;
    description: string;
    images: string[];
    creationAt: Date;
    updatedAt: Date;
    category: Category;
    quantity?: number;
}

export type Category = {
    id: number;
    name: string;
    image: string;
    creationAt: Date;
    updatedAt: Date;
}

export type ToasterType = 'success' | 'error' | 'info' | 'warning' | null;

export interface ToasterProps {
    message: string;
    type: ToasterType;
}


export interface GlobalContextProps {
    cart: Products[];
    productStates: Record<number, ProductState>;
    updateProductState: (productId: number, state: ProductState) => void;
    addToCart: (product: Products) => void;
    removeFromCart: (productId: number) => void;
    clearCart: () => void;
    updateProductQuantity: (productId: number, quantity: number) => void;
    showToast: (message: string, type: 'success' | 'error' | 'info' | 'warning') => void;
}