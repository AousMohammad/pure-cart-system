/**
 * BASE_URL Constant
 * 
 * The base URL for the API from which the application fetches data.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */
const BASE_URL = "https://api.escuelajs.co/api/v1";

/**
 * fetchInstance Function
 * 
 * A utility function to make API requests to the provided endpoint using the base URL.
 * It constructs the full URL, appends any provided parameters, and fetches the data.
 * 
 * Parameters:
 * - `endpoint`: The specific endpoint to which the request is made.
 * - `params` (optional): Any query parameters to be appended to the request URL.
 * 
 * Returns:
 * - A promise that resolves with the JSON data from the response.
 * 
 * Throws:
 * - An error if the network response is not ok.
 * 
 * Usage:
 * ```javascript
 * const data = await fetchInstance(ENDPOINTS.PRODUCTS, { limit: 10 });
 * ```
 * 
 * Dependencies:
 * - `BASE_URL`: The base URL for the API.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 * 
 * @param endpoint The specific endpoint for the API request.
 * @param params Optional query parameters for the request.
 */
export const fetchInstance = async (endpoint: string, params?: any) => {
    const url = new URL(`${BASE_URL}${endpoint}`);

    if (params) {
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    }

    const response = await fetch(url.toString());
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    return response.json();
};
