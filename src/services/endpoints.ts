/**
 * ENDPOINTS Constant
 * 
 * A collection of endpoint URLs used throughout the application for making API requests.
 * 
 * Structure:
 * - Each key represents the name of the resource or functionality.
 * - Each value is the corresponding endpoint URL for that resource or functionality.
 * 
 * Current Endpoints:
 * - `PRODUCTS`: Endpoint to fetch the list of products.
 * 
 * Usage:
 * ```javascript
 * const productsURL = ENDPOINTS.PRODUCTS;
 * ```
 * 
 * Note:
 * - As the application grows, additional endpoints can be added to this constant for better organization and central management.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

export const ENDPOINTS = {
    PRODUCTS: "/products"
};
