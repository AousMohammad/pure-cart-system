/**
 * useProducts Custom Hook
 * 
 * A custom hook that fetches a list of products from a specified endpoint.
 * It provides functionalities to handle pagination using offset and limit parameters.
 * 
 * Features:
 * - Fetches products from the server.
 * - Handles loading state, error state, and successful data fetching.
 * - Provides pagination support using offset and limit.
 * 
 * Parameters:
 * - `offset`: The starting index for fetching products. Default is 0.
 * - `limit`: The maximum number of products to fetch. Default is 10.
 * 
 * Returns:
 * - `products`: An array of fetched products.
 * - `loading`: A boolean indicating the loading state.
 * - `error`: A string indicating any error messages, or null if no errors.
 * 
 * Usage:
 * const { products, loading, error } = useProducts({ offset: 5, limit: 15 });
 * 
 * Dependencies:
 * - `Products`: Type definitions for products.
 * - `fetchInstance`: A function to make API calls.
 * - `ENDPOINTS`: An object containing API endpoint URLs.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import { useState, useEffect } from 'react';
import { fetchInstance, ENDPOINTS } from '../services';
import { Products } from '../types';
import { useGlobalContext } from '../context/GlobalContext';

interface UseProductsProps {
    offset?: number;
    limit?: number;
}

export const useProducts = ({ offset = 0, limit = 10 }: UseProductsProps) => {
    const [products, setProducts] = useState<Products[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string | null>(null);
    const { showToast } = useGlobalContext();

    useEffect(() => {
        const fetchProducts = async () => {
            setLoading(true);
            try {
                const response = await fetchInstance(ENDPOINTS.PRODUCTS, { offset, limit });
                setProducts(response);
            } catch (err) {
                setError((err as Error).message);
                showToast('Failed to fetch products!', 'error');
            } finally {
                setLoading(false);
            }
        };

        fetchProducts();
    }, [offset, limit, showToast]);

    return { products, loading, error };
};
