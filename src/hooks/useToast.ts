/**
 * useToast Custom Hook
 * 
 * A custom hook that provides functionality to display toast notifications.
 * It manages the toast message, its type, and the visibility duration.
 * 
 * Features:
 * - Displays toast notifications with a specified message and type.
 * - Automatically hides the toast after a 3-second duration.
 * 
 * Returns:
 * - `toastMessage`: The current message of the toast. Null if no toast is being displayed.
 * - `toastType`: The type of the toast (e.g., 'success', 'error'). Null if no toast is being displayed.
 * - `showToast`: A function to display a toast with a specified message and type.
 * 
 * Usage:
 * const { toastMessage, toastType, showToast } = useToast();
 * showToast('Data saved successfully!', 'success');
 * 
 * Dependencies:
 * - `ToasterType`: Type definitions for the different types of toasts.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import { useState, useCallback } from 'react';
import { ToasterType } from '../types';

export const useToast = () => {
    const [toastMessage, setToastMessage] = useState<string | null>(null);
    const [toastType, setToastType] = useState<ToasterType | null>(null);

    const showToast = useCallback((message: string, type: ToasterType) => {
        setToastMessage(message);
        setToastType(type);
        setTimeout(() => {
            setToastMessage(null);
            setToastType(null);
        }, 3000);
    }, []);

    return { toastMessage, toastType, showToast };
};
