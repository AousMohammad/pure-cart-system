/**
 * useCart Custom Hook
 * 
 * A custom hook that provides cart-related functionalities such as adding products to the cart,
 * removing products from the cart, clearing the cart, and updating product quantities.
 * 
 * Features:
 * - Maintains the state of the cart.
 * - Provides methods to manipulate the cart.
 * - Integrates with external functions to show toast messages and update product states.
 * 
 * Parameters:
 * - `showToast`: A function to display toast notifications. It accepts a message and a type (e.g., 'success', 'error').
 * - `updateProductState`: A function to update the state of a product (e.g., NORMAL, ADDED, BOUGHT).
 * 
 * Returns:
 * - `cart`: The current state of the cart.
 * - `addToCart`: A function to add a product to the cart.
 * - `removeFromCart`: A function to remove a product from the cart by its ID.
 * - `clearCart`: A function to clear all products from the cart.
 * - `updateProductQuantity`: A function to update the quantity of a product in the cart by its ID.
 * 
 * Usage:
 * const { cart, addToCart, removeFromCart, clearCart, updateProductQuantity } = useCart(showToastFunction, updateProductStateFunction);
 * 
 * Dependencies:
 * - `Products`: Type definitions for products.
 * - `ProductState`: Type definitions for the state of a product (e.g., NORMAL, ADDED, BOUGHT).
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import { useState, useCallback } from 'react';
import { Products } from '../types';
import { ProductState } from '../components/Product/types';

export const useCart = (showToast: Function, updateProductState: Function) => {
    const [cart, setCart] = useState<Products[]>([]);

    const addToCart = useCallback((product: Products) => {
        if (!cart.some(p => p.id === product.id)) {
            setCart(prevCart => [...prevCart, { ...product, quantity: 1 }]);
            showToast('Item added to cart!', 'success');
        } else {
            showToast('Product is already in the cart!', 'error');
        }
    }, [showToast, cart]);

    const removeFromCart = useCallback((productId: number) => {
        setCart(prevCart => prevCart.filter(product => product.id !== productId));
        updateProductState(productId, ProductState.NORMAL);
        showToast('Item removed from cart!', 'success');
    }, [showToast, updateProductState]);

    const clearCart = useCallback(() => {
        cart.forEach(product => {
            updateProductState(product.id, ProductState.BOUGHT);
        });
        setCart([]);
    }, [cart, updateProductState]);

    const updateProductQuantity = useCallback((productId: number, quantity: number) => {
        setCart(prevCart =>
            prevCart.map(product =>
                product.id === productId ? { ...product, quantity } : product
            )
        );
    }, []);

    return { cart, addToCart, removeFromCart, clearCart, updateProductQuantity };
};
