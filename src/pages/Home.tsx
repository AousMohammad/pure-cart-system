/**
 * Home Component
 * 
 * The main landing page of the application that displays a list of products with pagination support.
 * 
 * Features:
 * - Utilizes the `useProducts` hook to fetch a list of products based on the current page.
 * - Displays a loading spinner (`Loader` component) while the products are being fetched.
 * - Renders the fetched products using a memoized version of the `ProductList` component to optimize performance.
 * - Uses the `ErrorBoundary` component to gracefully handle any errors that might occur during rendering or fetching.
 * - Wrapped within the `SidebarProvider` to provide sidebar-related functionalities to its child components.
 * - Supports pagination using the `Pagination` component, allowing users to navigate through different pages of products.
 * 
 * Props:
 * - None.
 * 
 * Returns:
 * - A component that displays a list of products or a loading spinner based on the fetching status, along with pagination controls.
 * 
 * Usage:
 * ```jsx
 * <Home />
 * ```
 * 
 * Dependencies:
 * - `ErrorBoundary`: Component to catch and display errors in a user-friendly manner.
 * - `Layout`: A layout component that structures the main content of the page.
 * - `Loader`: Component to display a loading spinner.
 * - `ProductList`: Component to display a list of products.
 * - `Pagination`: Component to display and handle pagination controls.
 * - `useProducts`: Hook to fetch and manage the list of products based on pagination.
 * - `SidebarProvider`: Context provider for sidebar-related functionalities.
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */

import React from 'react'
import { Layout, Loader, Pagination, ProductList } from '../components'
import { useProducts } from '../hooks';
import { SidebarProvider } from '../context';
import { ErrorBoundary } from '../components/ErrorBoundary';

const MemoizedProductList = React.memo(ProductList);

const Home = () => {
    const [currentPage, setCurrentPage] = React.useState(1);
    const itemsPerPage = 10;
    const totalProducts = 100;
    const { products, loading } = useProducts({ offset: (currentPage - 1) * itemsPerPage, limit: itemsPerPage });
    return (
        <SidebarProvider>
            <ErrorBoundary>
                <Layout>
                    {loading ? <Loader /> :
                        <>
                            <MemoizedProductList products={products} />
                            <Pagination
                                currentPage={currentPage}
                                totalItems={totalProducts}
                                itemsPerPage={itemsPerPage}
                                onPageChange={setCurrentPage}
                            />
                        </>
                    }
                </Layout>
            </ErrorBoundary>
        </SidebarProvider>
    )
}

export default Home