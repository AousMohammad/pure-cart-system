/**
 * simulateApiDelay Utility Function
 * 
 * This utility function simulates an API delay by returning a promise that resolves after a specified duration.
 * 
 * @param {number} duration - The duration (in milliseconds) for which the delay should last. Default is 1000ms.
 * 
 * @returns {Promise<void>} A promise that resolves after the specified duration.
 * 
 * Usage:
 * ```tsx
 * await simulateApiDelay(2000);  // Simulates a 2-second delay
 * ```
 * 
 * @author Aous Mohammad
 * @email aous.mohammad97@gmail.com
 */
export const simulateApiDelay = (duration: number = 1000) => {
    return new Promise(resolve => setTimeout(resolve, duration));
};
